var app = angular.module('myApp', ['ngRoute','ngTouch','swipe']);

app.controller("AppController", function($scope){
$scope.showmenu=false;
$scope.toggleMenu = function(){
$scope.showmenu=($scope.showmenu) ? false : true;
}

$scope.swipe = function($event) {
	console.log($event);
	//$scope.toggleMenu();
  };
 
});

$( document ).ready(function() {
  	
	var startX;
	var startTime;
	var distX;
	var elapsedTime;
	var isTouch;
    var	threshold = 10;
    var allowedTime = 100000;
	
	
	$('#wrapper,#menuleft').bind('touchstart',function(e){
		isTouch=true;
		var touchobj = e.changedTouches[0];
        startX = touchobj.pageX;
        startTime = new Date().getTime();
        e.preventDefault();
	}
    );
	
	$('#wrapper,#menuleft').bind('touchend',function(e){
		isTouch=false;
		distX=0;
		document.getElementById('testEvent').innerHTML='-4';
		e.preventDefault();
	}
	);
	
	
	$('#wrapper,#menuleft').bind('touchmove',function(e){
	   var touchobj = e.changedTouches[0];
        distX = touchobj.pageX - startX; 
        elapsedTime = new Date().getTime() - startTime; 

		if (isTouch) {
		   if (elapsedTime <= allowedTime){ 
            if (Math.abs(distX) >= threshold){
                var appElement = document.querySelector('[ng-app=myApp]');
                var $scope = angular.element(appElement).scope();
                $scope.$apply(function() {
					if ((!$scope.showmenu)&&(distX>0)) {
						$scope.showmenu=true;
						isTouch=false;
					}
					if (($scope.showmenu)&&(distX<0)) {
						$scope.showmenu=false;
						isTouch=false;
					}
					
                });
            }
       }
  }
	  
	  
});
});

